var dbConn = require("../config/db.config");

var User = function (user) {
  this.nom = user.nom;
  this.email = user.email;
  this.password = user.password;
};

User.register = function (newUser, result) {
  dbConn.query("INSERT INTO user set ?", newUser, function (err, res) {
    if (err) {
      result(err, null);
    } else {
      result(null, res.insertId);
    }
  });
};

User.find = function (email, result) {
  dbConn.query(
    "SELECT * FROM user where email = ? ",
    email,
    function (err, res) {
      if (err) {
        result(err, null);
      } else {
        result(null, res);
      }
    }
  );
};

module.exports = User;
