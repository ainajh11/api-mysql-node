"use strict";
const User = require("../models/user.model");
const bcrypt = require("bcrypt");

module.exports.register = async (req, res) => {
  const { nom, email, password } = req.body;
  const salt = await bcrypt.genSalt();
  const hashPassword = await bcrypt.hash(password, salt);
  const new_user = {
    nom,
    email,
    password: hashPassword,
  };

  if (req.body.constructor === Object && Object.keys(req.body).length === 0) {
    res
      .status(400)
      .send({ error: true, message: "Please provide all required field" });
  } else {
    User.register(new_user, (err, user) => {
      if (err) res.status(400).send(err);
      res.json({
        message: "Register successfully!",
        data: user,
      });
    });
  }
};

module.exports.signIn = (req, res) => {
  const { email, password } = req.body;
  User.find(email, async (err, data) => {
    if (data.length != 0) {
      const pass = String(data[0].password);
      const auth = await bcrypt.compare(password, pass);
      if (auth) {
        return res
          .status(200)
          .send({ message: `Hello ${data[0].nom}! You are connected` });
      } else {
        return res.status(400).send({ message: "password incorrect" });
      }
    } else {
      res.status(400).json({ message: "Email incorrect" });
    }
  });
};
