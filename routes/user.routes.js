const router = require("express").Router();
const userController = require("../controllers/user.controller");

router.post("/register", userController.register);
router.get("/login", userController.signIn);

module.exports = router;
